\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{idial-proposal}[2023-03-01 v1.0.0 IDiAL proposal class]
% Language options
\DeclareOption{ngerman}{\PassOptionsToPackage{\CurrentOption}{babel}}
\DeclareOption{english}{\PassOptionsToPackage{main=\CurrentOption,ngerman}{babel}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrbook}}
\ProcessOptions\relax

% used to detect language families
\RequirePackage{translations}

\LoadClass{scrartcl}

%% VARIABLES
\newcommand{\theidnumber}{}
\newcommand{\idnumber}[1]{\renewcommand{\theidnumber}{#1}}
\newcommand{\theemail}{}
\newcommand{\email}[1]{\renewcommand{\theemail}{#1}}

\newcommand{\thereviewerone}{}
\newcommand{\thereviewertwo}{}
\newcommand{\reviewerone}[1]{\renewcommand{\thereviewerone}{#1}}
\newcommand{\reviewertwo}[1]{\renewcommand{\thereviewertwo}{#1}}

\newcommand{\thekind}{}
\newcommand{\kind}[1]{\renewcommand{\thekind}{#1}}

\newcommand{\thedegree}{}
\newcommand{\degree}[1]{\renewcommand{\thedegree}{#1}}

%% TYPOGRAPHY

% T1 font encoding
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}

\RequirePackage{lmodern}
\RequirePackage[sc]{mathpazo}
\linespread{1.05} % adjust line spread for mathpazo font
% microtype for nicer typography
\RequirePackage{microtype}

% semi-bold type (for subsections and paragraphs)
\newcommand*{\sbdefault}{sb}
\DeclareRobustCommand{\sbseries}{%
    \not@math@alphabet\sbseries\relax
    \fontseries\sbdefault\selectfont}

\DeclareTextFontCommand{\textsb}{\sbseries}

\addtokomafont{subsection}{\sbseries}
\addtokomafont{subsubsection}{\sbseries}
\addtokomafont{paragraph}{\sbseries}

%% SPACING

\RedeclareSectionCommands[
    beforeskip= .7em plus .6em minus .3em
]{paragraph}

%% PAGE LAYOUT
\KOMAoptions{
    fontsize=11pt,
    paper=a4, 
    twoside=false,
    titlepage=false,
    headinclude=true,
    footinclude=true,
}

\RequirePackage[left=2.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm]{geometry}

\RequirePackage[onehalfspacing]{setspace}


\clubpenalty=10000 % prevent orphans
\widowpenalty=10000 % prevent widows

%% Styling of titlepage

\RequirePackage{titling}
\setkomafont{title}{\huge\sffamily\bfseries}

\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[L]{\@title}
\fancyhead[R]{\@author}

%internal commands
\renewcommand{\maketitle}{%
\thispagestyle{empty}
\begin{center}
    \vskip0.2em{\titlefont\LARGE\@title\par}\vskip1.0em\par%
    {\@author\ -- \theidnumber\par \theemail\par}
\end{center}
\vskip1.5em
}

%% PACKAGES
\RequirePackage{amssymb}
\RequirePackage{babel}
\RequirePackage{csquotes}

\RequirePackage[shortcuts]{extdash}

% tables
\RequirePackage{booktabs}
\RequirePackage{longtable}
\RequirePackage{array}

\RequirePackage{graphicx}

% appendix
\RequirePackage[toc,title,header]{appendix}
\noappendicestocpagenum

% PDF specific packages
\RequirePackage[hyphens]{url}
\RequirePackage[breaklinks,colorlinks=false,hidelinks]{hyperref}

% Settings for lstlistings
\RequirePackage{xcolor}
\definecolor{FHOrange}{cmyk}{0,70,100,0}
\definecolor{AccentGreen}{cmyk}{6, 0, 100, 32}

\RequirePackage{scrhack} % necessary for listings package
\RequirePackage{listings}

\renewcommand*\lstlistlistingname{\ifcurrentbaselanguage{English}{List of Listings}{Quellcodeverzeichnis}}

\lstset{%
  basicstyle=\ttfamily,
  columns=fullflexible,
  keywordstyle=\bfseries\color{FHOrange},
  stringstyle=\color{AccentGreen}
}